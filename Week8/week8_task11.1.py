from itertools import combinations, permutations

a = b = 1


print(
    *map(
        lambda x: list(
            combinations(
                x,
                2
            )
        ),
        permutations(
                range(
                    1,
                    int(
                        input()[0]
                    ) + 1
                )
            )
    )
)

# print(*permutations([1, 2, 3]))

# print(*map(lambda x: list(combinations(x, 2)), permutations([1, 2, 3])))

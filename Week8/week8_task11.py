from itertools import combinations, permutations


def cockroach_racing(k, bets):
    for i in permutations(range(1, k + 1)):
        comb = list(combinations(i, 2))
        q = 0

        for key in bets:
            if bets[key][0] in comb and bets[key][1] not in comb:
                q += 1
            elif bets[key][0] not in comb and bets[key][1] in comb:
                q += 1

        if q == len(bets):
            print(*reversed(i))
            return

    print(0)

    return


def main():
    temp = list(map(int, input().split()))
    k = temp[0]
    n = temp[1]
    bets = dict()

    for i in range(n):
        temp = list(map(int, input().split()))

        if i not in bets:
            bets[i] = []
        bets[i].append((temp[0], temp[1]))
        bets[i].append((temp[2], temp[3]))

    cockroach_racing(k, bets)


if __name__ == '__main__':
    main()

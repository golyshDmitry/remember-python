def nod(a, b):

    if b == 0:
        return a

    return nod(b, a % b)


def main():
    a, b = int(input()), int(input())

    print(nod(a, b))


if __name__ == "__main__":
    main()

def min_divisor(n):
    i = 2

    while i <= int(n ** 0.5):
        if n % i == 0:
            n = i
            break
        i += 1

    return n


def main():
    n = int(input())

    print(min_divisor(n))


if __name__ == "__main__":
    main()

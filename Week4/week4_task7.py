def is_point_in_area(x, y):

    if y < 0:
        if -0.6 - 0.2 * 19**0.5 <= x <= -1 + 2**0.5:
            if (y - 2) / 2 <= x <= -y and y <= 1 - (4 - (x + 1)**2)**0.5:
                return True
        elif (y - 2) / 2 <= x <= -y:
            return True

    if y >= 1 and -1 - 2**0.5 <= x <= -0.6 + 0.2 * 19**0.5:
        if -y <= x <= (y - 2) / 2 and y <= (4 - (x + 1)**2)**0.5 + 1:
            return True

    if 2 / 3 <= y < 1:
        if -y <= x <= (y - 2) / 2 and y <= 1 - (4 - (x + 1)**2)**0.5:
            return True


def main():
    x, y = float(input()), float(input())

    if is_point_in_area(x, y):
        print("YES")
    else:
        print("NO")


if __name__ == "__main__":
    main()

def reverse_squares(k=0):
    n = int(input())

    if n != 0:
        if int(n ** 0.5) == n ** 0.5:
            reverse_squares(k + 1)
            print(n, end=' ')

        else:
            reverse_squares(k)

    if n == 0 and k == 0:
        print(k)
    elif n == 0:
        return


def main():
    reverse_squares()


if __name__ == "__main__":
    main()

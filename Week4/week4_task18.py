def combinations(n, k):

    if k == 0:
        return 1

    if k == 1:
        return n

    if n == k:
        return 1

    return combinations(n - 1, k) + combinations(n - 1, k - 1)


def main():
    n, k = int(input()), int(input())

    print(combinations(n, k))


if __name__ == "__main__":
    main()

def is_prime(n):
    i = 2

    while i <= int(n ** 0.5):
        if n % i == 0:
            return False
        i += 1

    return True


def main():
    n = int(input())

    if is_prime(n):
        print("YES")
    else:
        print("NO")


if __name__ == "__main__":
    main()

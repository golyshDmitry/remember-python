def cubes(n):
    i = 1
    a = []

    while i ** 3 <= n:
        a.append(i ** 3)
        i += 1

    length = len(a) - 1
    i = j = k = t = q = w = e = length

    while i >= 0:
        if check_cube_sum(n, a[i]) == "equal":
            print(a[i])
            return

        while j >= 0:
            s = check_cube_sum(n, a[i], a[j])

            if s == "equal":
                print(a[i], a[j])
                return
            elif s == "more":
                j -= 1
                continue

            while k >= 0:
                s = check_cube_sum(n, a[i], a[j], a[k])

                if s == "equal":
                    print(a[i], a[j], a[k])
                    return
                elif s == "more":
                    k -= 1
                    continue

                while t >= 0:
                    s = check_cube_sum(n, a[i], a[j], a[k], a[t])

                    if s == "equal":
                        print(a[i], a[j], a[k], a[t])
                        return
                    elif s == "more":
                        t -= 1
                        continue

                    while q >= 0:
                        s = check_cube_sum(n, a[i], a[j], a[k], a[t], a[q])

                        if s == "equal":
                            print(a[i], a[j], a[k], a[t], a[q])
                            return
                        elif s == "more":
                            q -= 1
                            continue

                        while w >= 0:
                            s = check_cube_sum(n, a[i], a[j], a[k], a[t],
                                               a[q], a[w])

                            if s == "equal":
                                print(a[i], a[j], a[k], a[t], a[q], a[w])
                                return
                            elif s == "more":
                                w -= 1
                                continue

                            while e >= 0:
                                s = check_cube_sum(n, a[i], a[j], a[k], a[t],
                                                   a[q], a[w], a[e])

                                if s == "equal":
                                    print(a[i], a[j], a[k], a[t], a[q], a[w],
                                          a[e])
                                    return
                                elif s == "more":
                                    e -= 1
                                    continue

                                e -= 1

                            w -= 1
                            e = length

                        q -= 1
                        w = e = length

                    t -= 1
                    q = w = e = length

                k -= 1
                t = q = w = e = length

            j -= 1
            k = t = q = w = e = length

        i -= 1
        j = k = t = q = w = e = length

    print(0)


def check_cube_sum(n, *params):
    s = 0

    for i in params:
        s += i

    if s == n:
        return "equal"
    elif s > n:
        return "more"
    else:
        return "less"


def main():
    n = int(input())

    cubes(n)


if __name__ == "__main__":
    main()

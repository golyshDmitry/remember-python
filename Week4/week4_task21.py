def hanoy_towers(n, from_=1, to=3, buff=2):

    if n == 0:
        return

    hanoy_towers(n - 1, from_, buff, to)

    print(n, from_, to)

    hanoy_towers(n - 1, buff, to, from_)


def main():
    n = int(input())

    hanoy_towers(n)


if __name__ == "__main__":
    main()

def n_fibonacci(n, temp=1, prev=0):

    if n == 0:
        return prev

    return n_fibonacci(n - 1, temp + prev, temp)


def main():
    n = int(input())

    print(n_fibonacci(n))


if __name__ == "__main__":
    main()

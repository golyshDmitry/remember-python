def pow_(a, n):
    if n == 0:
        return 1

    return a * pow_(a, n - 1)


def main():
    a, n = float(input()), int(input())

    print(pow_(a, n))


if __name__ == "__main__":
    main()

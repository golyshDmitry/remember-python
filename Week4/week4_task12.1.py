def pow_(a, n):
    i = 0
    p = a

    if n == 0:
        return 1

    while i < n - 1:
        p *= a
        i += 1

    return p


def main():
    a, n = float(input()), int(input())

    if n >= 0:
        print(pow_(a, n))

    else:
        print(1 / pow_(a, abs(n)))


if __name__ == "__main__":
    main()

def pow_(a, n):

    if n == 0:
        return 1

    if n % 2 != 0:
        return a * pow_(a, n - 1)
    else:
        return pow_(a * a, n // 2)


def main():
    a, n = float(input()), int(input())

    print(pow_(a, n))


if __name__ == "__main__":
    main()

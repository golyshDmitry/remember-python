def reduce_fraction(n, m):
    z = nod(n, m)

    return n // z, m // z


def nod(a, b):
    if b == 0:
        return a

    return nod(b, a % b)


def main():
    n, m = int(input()), int(input())

    print(*reduce_fraction(n, m))


if __name__ == "__main__":
    main()

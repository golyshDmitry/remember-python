def sum_(s=0):
    n = int(input())

    if n != 0:
        s += n
        return sum_(s)
    else:
        return s


def main():
    print(sum_())


if __name__ == "__main__":
    main()

def sum_(a, b):

    if b == 0:
        return a

    return sum_(a + 1, b - 1)


def main():
    a, b = int(input()), int(input())

    print(sum_(a, b))


if __name__ == "__main__":
    main()

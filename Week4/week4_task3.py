def distance(a, b, c, d):
    return ((c - a) ** 2 + (d - b) ** 2) ** 0.5


def main():
    x1, y1, x2, y2, x3, y3 = float(input()), float(input()), \
                     float(input()), float(input()), float(input()), \
                     float(input())

    p = distance(x1, y1, x2, y2) + distance(x1, y1, x3, y3) + \
        distance(x2, y2, x3, y3)

    print('%.6f' % p)


if __name__ == "__main__":
    main()

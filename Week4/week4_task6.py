def is_point_in_circle(x, y, xc, yc, r):
    return ((x - xc) ** 2 + (y - yc) ** 2) ** 0.5 <= r


def main():
    x, y, xc, yc, r = float(input()), float(input()), float(input()), \
                      float(input()), float(input())

    if is_point_in_circle(x, y, xc, yc, r):
        print("YES")
    else:
        print("NO")


if __name__ == "__main__":
    main()

def xui(n):

    if int(n ** 0.5) == n ** 0.5:
        print(int(n ** 0.5))
        return

    else:
        sq = i = int(n ** 0.5)
        j = k = t = 1

        while i > 0:
            if check_sum(i, j, k, t, n):
                return

            while j <= sq:
                if check_sum(i, j, k, t, n):
                    return

                while k <= sq:
                    if check_sum(i, j, k, t, n):
                        return

                    while t <= sq:
                        if check_sum(i, j, k, t, n):
                            return

                        t += 1

                    k += 1
                    t = 1

                j += 1
                k = t = 1

            i -= 1
            j = k = t = 1


def check_sum(i, j, k, t, n):

    if i ** 2 + j ** 2 == n:
        print(i, j)
        return True

    elif i ** 2 + k ** 2 == n:
        print(i, k)
        return True

    elif i ** 2 + t ** 2 == n:
        print(i, t)
        return True

    elif j ** 2 + k ** 2 == n:
        print(j, k)
        return True

    elif j ** 2 + t ** 2 == n:
        print(j, t)
        return True

    elif k ** 2 + t ** 2 == n:
        print(k, t)
        return True

    elif i ** 2 + j ** 2 + k ** 2 == n:
        print(i, j, k)
        return True

    elif i ** 2 + j ** 2 + t ** 2 == n:
        print(i, j, t)
        return True

    elif i ** 2 + k ** 2 + t ** 2 == n:
        print(i, k, t)
        return True

    elif j ** 2 + k ** 2 + t ** 2 == n:
        print(j, k, t)
        return True

    elif i ** 2 + j ** 2 + k ** 2 + t ** 2 == n:
        print(i, j, k, t)
        return True

    else:
        return False


def main():
    n = int(input())

    xui(n)


if __name__ == "__main__":
    main()

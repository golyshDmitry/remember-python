def xor(x, y):
    if x + y == 1:
        return True
    return False


def main():
    x, y = int(input()), int(input())

    if xor(x, y):
        print(1)
    else:
        print(0)


if __name__ == "__main__":
    main()

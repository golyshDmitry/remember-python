def min_4(a, b, c, d):
    return min(min(a, b), min(c, d))


def main():
    a, b, c, d = int(input()), int(input()), int(input()), int(input())

    print(min_4(a, b, c, d))


if __name__ == "__main__":
    main()

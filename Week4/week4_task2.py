def distance(a, b, c, d):
    return ((c - a) ** 2 + (d - b) ** 2) ** 0.5


def main():
    x1, y1, x2, y2 = float(input()), float(input()), \
                     float(input()), float(input())

    print(distance(x1, y1, x2, y2))


if __name__ == "__main__":
    main()

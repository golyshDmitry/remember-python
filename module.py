'''a = [1, 2, 1]

a = sorted(a, key=lambda x: a.count(x))

print(a)'''

'''a = [1, 2, 1]

print(sorted(a, key=a.count))'''

'''a = [1, 2, 3]
b = ["yes"]
a.append(b)
print(a)
b.append(9)
print(a)'''


'''day = int(input())

if day % 6 != 0 and day % 7 != 0:
    print("no weekend")
else:
    print("weekend")
'''

'''
myDict = dict()
myDict[5] = "uxi"
print(myDict)
try:
    del myDict[8]
except KeyError:
    print("no such key")

print(myDict)

d = {'first': 'a', frozenset({2}): 'b', 3: 'c'}

for i in d:
    print(i, '-', d[i])

print()

for i, j in d.items():
    print(i, j)

print()

for i in d.keys():
    print(i)

print()

for i in d.values():
    print(i)

print()

a = {1: {1: 'a', 2: 'b'},
     2: {'a': 1, 'b': 2}}

for i in a:
    for j in a[i]:
        print(j, '-', a[i][j])
    print(i, '-', a[i])

print()

v = {1: 'a'}
print(v)
v[2] = 'b'
print(v)
v.pop(1)
print(v)

print()

c = {}
c.setdefault('l', 'left')
print(c)

print()

n = dict([(1, 2), ('a', 'b')])
print(n)

print()

r = dict()
print(r)

print(r.get(1, 'def'))
print(r)
# print(r[5])
print(r.get(5))

print()

y = [1, 2, 3]
print(str(y))

print()

s = input()
print(s.strip())
'''

'''a = [1, 2]
b = a
b[0] = 3
print(a)'''

# print(*range(int(input())))

'''a = iter([1, 2, 3])

i = next(a)
print(i)
i = next(a)
print(i)'''


'''class Complex:
    counter = 0

    def __init__(self, re=0, im=0):
        self.re = re
        self.im = im
        Complex.counter += 1


number_one = Complex(im=2, re=1)

print(number_one.re, ' + ', number_one.im, 'i', sep='')

print(number_one.counter)

number_two = Complex()
print(number_two.counter)'''

a = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

b = list()

for i in range(len(a[0])):
    b.append([])
    for j in range(len(a)):
        b[i].append(a[j][i])

a = b

for i in range(len(a)):
    for j in range(len(a[0])):
        print(a[i][j], '', end='')
    print()

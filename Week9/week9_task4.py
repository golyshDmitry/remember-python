from sys import stdin


class MatrixError(BaseException):
    def __init__(self, first, second):
        self.matrix1 = first
        self.matrix2 = second


class Matrix:
    def __init__(self, a):
        self.matrix = [x[:] for x in a]

    def __str__(self):
        str_matrix = ""
        for i in self.matrix:
            for j in i:
                str_matrix += str(j) + '\t'
            str_matrix = str_matrix[:-1]
            str_matrix += '\n'

        str_matrix = str_matrix[:-1]

        return str_matrix

    def size(self):
        return len(self.matrix), len(self.matrix[0])

    def __add__(self, other):
        if len(self.matrix) == len(other.matrix) and \
                len(self.matrix[0]) == len(other.matrix[0]):
            temp = list()
            for i in range(len(self.matrix)):
                temp.append([])
                for j in range(len(self.matrix[0])):
                    temp[i].append(self.matrix[i][j] + other.matrix[i][j])
            return Matrix(temp)
        else:
            raise MatrixError(self, other)

    def __mul__(self, x):
        temp = list()
        for i in range(len(self.matrix)):
            temp.append([])
            for j in self.matrix[i]:
                temp[i].append(j * x)

        return Matrix(temp)

    __rmul__ = __mul__

    def transpose(self):
        temp = list()
        for i in range(len(self.matrix[0])):
            temp.append([])
            for j in range(len(self.matrix)):
                temp[i].append(self.matrix[j][i])
        self.matrix = temp

        return self

    @staticmethod
    def transposed(obj):
        temp = Matrix(obj.matrix)
        Matrix.transpose(temp)
        return temp


exec(stdin.read())

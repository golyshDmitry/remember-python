from sys import stdin


class Matrix:
    def __init__(self, a):
        self.matrix = [x[:] for x in a]

    def __str__(self):
        str_matrix = ""
        for i in self.matrix:
            for j in i:
                str_matrix += str(j) + '\t'
            str_matrix = str_matrix[:-1]
            str_matrix += '\n'

        str_matrix = str_matrix[:-1]

        return str_matrix

    def size(self):
        return len(self.matrix), len(self.matrix[0])


exec(stdin.read())

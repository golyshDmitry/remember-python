def count_pairs(_list):
    k = i = 0
    length = len(_list)

    _list.sort()

    while i < length:
        rep = _list.count(_list[i])
        if rep > 1:
            k += combinations(2, rep)
        i += rep

    return k


def combinations(k, n):

    return int(fact(n) / (fact(k) * fact(n - k)))


def fact(n):
    if n == 0:
        return 1

    return n * fact(n - 1)


def main():
    _list = list(map(int, input().split()))

    print(count_pairs(_list))


if __name__ == "__main__":
    main()

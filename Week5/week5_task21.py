def first_max(_list):
    index = 0
    temp_max = _list[0]

    for i in range(len(_list)):
        if _list[i] > temp_max:
            temp_max = _list[i]
            index = i

    return temp_max, index


def main():
    _list = list(map(int, input().split()))

    print(* first_max(_list))


if __name__ == "__main__":
    main()

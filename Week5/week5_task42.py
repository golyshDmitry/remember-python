def most_popular(_list):
    k = _list[0]
    q = _list.count(_list[0])

    for i in range(1, len(_list)):
        if _list.count(_list[i]) > q:
            k = _list[i]
            q = _list.count(_list[i])

    return k


def main():
    _list = list(map(int, input().split()))

    print(most_popular(_list))


if __name__ == '__main__':
    main()

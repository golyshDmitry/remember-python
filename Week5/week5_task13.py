def even_index():

    myList = list(input().split())

    for i in range(len(myList)):
        if i % 2 == 0:
            print(myList[i], end=' ')


def main():
    even_index()


if __name__ == "__main__":
    main()

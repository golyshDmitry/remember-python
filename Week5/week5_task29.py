def define_place(_list, h):

    _list.append(h)
    _list.sort(reverse=True)

    return _list.index(h) + _list.count(h)


def main():
    _list = list(map(int, input().split()))
    h = int(input())

    print(define_place(_list, h))


if __name__ == "__main__":
    main()

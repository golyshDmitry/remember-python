def same_signs(_list):
    length = len(_list)

    for i in range(length - 1):
        if _list[i] * _list[i + 1] > 0 or _list[i] == _list[i + 1] == 0:
            print(_list[i], _list[i + 1])
            return

    return


def main():
    _list = list(map(int, input().split()))

    same_signs(_list)


if __name__ == "__main__":
    main()

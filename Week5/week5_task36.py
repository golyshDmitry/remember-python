def max_composition_of_three(_list):

    if len(_list) <= 3:
        return _list

    if len(_list) == 4:
        p = multiply_list(_list)
        p_list = []

        if p == 0:
            _list.remove(0)
            p = multiply_list(_list)
            if p > 0:
                return _list
            else:
                return _list[0], _list[1], 0

        for i in _list:
            p_list.append(p // i)

        _list.pop(p_list.index(max(p_list)))

        return _list

    m1 = _list.pop(_list.index(max(_list)))
    n1 = _list.pop(_list.index(min(_list)))

    m2 = _list.pop(_list.index(max(_list)))
    n2 = _list.pop(_list.index(min(_list)))

    m3 = max(_list)
    # n3 = min(_list)

    if m1 * m2 * m3 >= m1 * n1 * n2:
        return m1, m2, m3
    else:
        return m1, n1, n2


def multiply_list(a):
    p = 1

    for i in a:
        p *= i

    return p


def main():
    _list = list(map(int, input().split()))

    print(*max_composition_of_three(_list))


if __name__ == "__main__":
    main()

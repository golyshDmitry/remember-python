def cyclic_shift(_list):

    _list.insert(0, _list[-1])
    _list.pop()

    return _list


def main():
    _list = list(map(int, input().split()))

    print(*cyclic_shift(_list))


if __name__ == "__main__":
    main()

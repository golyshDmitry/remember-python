def more_than_previous(_list):

    for i in range(1, len(_list)):
        if _list[i] > _list[i - 1]:
            print(_list[i], end=' ')


def main():
    myList = list(map(int, input().split()))

    more_than_previous(myList)


if __name__ == "__main__":
    main()

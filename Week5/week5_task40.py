def queens(desk):
    s = 0

    for i in range(8):
        for j in range(8):
            s += desk[i][j]
        if s > 1:
            return "YES"
        s = 0

    for j in range(8):
        for i in range(8):
            s += desk[i][j]
        if s > 1:
            return "YES"
        s = 0

    for i in range(8):
        for j in range(i, -1, -1):
            s += desk[j][i - j]
        if s > 1:
            return "YES"
        s = 0

    for i in range(7, 0, -1):
        for j in range(i, 8):
            s += desk[j][7 - j + i]
        if s > 1:
            return "YES"
        s = 0

    for i in range(7, 0, -1):
        for j in range(i, 8):
            s += desk[j - i][j]
        if s > 1:
            return "YES"
        s = 0

    for i in range(7):
        for j in range(i, -1, -1):
            s += desk[7 - i + j][j]
        if s > 1:
            return "YES"
        s = 0

    return "NO"


def print_desk(desk):
    for i in range(8):
        for j in range(8):
            print(desk[i][j], end=' ')
        print()
    print()


def main():
    desk = [[0] * 8 for i in range(8)]

    for i in range(8):
        temp = list(map(int, input().split()))
        desk[temp[0] - 1][temp[1] - 1] = 1

    print(queens(desk))


if __name__ == "__main__":
    main()

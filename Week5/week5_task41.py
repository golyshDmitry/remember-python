def compress_list(_list):
    not_null = len(_list) - _list.count(0)

    for i in range(not_null):
        if _list[i] == 0:
            for j in range(i + 1, len(_list)):
                if _list[j] != 0:
                    _list[j], _list[i] = _list[i], _list[j]
                    break

    return _list


def main():
    _list = list(map(int, input().split()))

    print(*compress_list(_list))


if __name__ == '__main__':
    main()

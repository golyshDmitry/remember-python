def lost_card(n):
    s = (1 + n) / 2 * n

    for i in range(n - 1):
        s -= int(input())

    return int(s)


def main():
    n = int(input())

    print(lost_card(n))


if __name__ == "__main__":
    main()

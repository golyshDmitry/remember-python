def more_than_neighbors(_list):
    length = len(_list)
    k = 0

    if length < 2:
        return k

    for i in range(1, length - 1):
        if _list[i] > _list[i - 1] and _list[i] > _list[i + 1]:
            k += 1

    return k


def main():
    _list = list(map(int, input().split()))

    print(more_than_neighbors(_list))


if __name__ == "__main__":
    main()

def swap_min_max(_list):

    m = _list.index(max(_list))
    n = _list.index(min(_list))

    _list[m], _list[n] = _list[n], _list[m]

    return _list


def main():
    _list = list(map(int, input().split()))

    print(*swap_min_max(_list))


if __name__ == "__main__":
    main()

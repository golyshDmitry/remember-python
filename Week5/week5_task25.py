def reverse_list(_list):

    for i in range(len(_list) // 2):
        temp = _list[i]
        _list[i] = _list[len(_list) - 1 - i]
        _list[len(_list) - 1 - i] = temp

    print(' '.join(map(str, _list)))


def main():
    _list = list(map(int, input().split()))

    reverse_list(_list)


if __name__ == "__main__":
    main()

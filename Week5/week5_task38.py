def unique_elements(_list):
    unique = []

    for i in _list:
        if _list.count(i) == 1:
            unique.append(i)

    return unique


def main():
    _list = list(map(int, input().split()))

    print(*unique_elements(_list))


if __name__ == "__main__":
    main()

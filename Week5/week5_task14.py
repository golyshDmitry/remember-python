def even_numbers():

    myList = list(map(int, input().split()))

    for i in myList:
        if i % 2 == 0:
            print(i, end=' ')


def main():
    even_numbers()


if __name__ == "__main__":
    main()

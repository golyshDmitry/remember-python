def print_flag(n):

    flag1 = "+___ "
    flag3 = "|__\\ "
    flag4 = "|    "

    print(flag1 * n)

    for i in range(1, n + 1):
        flag2 = "|" + str(i) + " / "
        print(flag2, end='')

    print('\n', flag3 * n, sep='')
    print(flag4 * n)


def main():
    n = int(input())

    print_flag(n)


if __name__ == "__main__":
    main()

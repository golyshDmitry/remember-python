def cool_numbers(a, b):

    for i in range(a, b + 1):
        if i // 1000 == i % 10 and (i // 10) % 10 == (i // 100) % 10:
            print(i)


def main():
    a, b = int(input()), int(input())

    cool_numbers(a, b)


if __name__ == "__main__":
    main()

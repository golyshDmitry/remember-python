def nearest_element(_list, el):
    k = 2001
    q = 0

    for i in _list:
        if abs(i - el) < k:
            k = abs(i - el)
            q = i
        if k == 0:
            break

    return q


def main():
    n = int(input())

    _list = list(map(int, input().split()))

    el = int(input())

    print(nearest_element(_list, el))


if __name__ == "__main__":
    main()

def roots_number():
    a, b, c, d, e = int(input()), int(input()), int(input()), \
                    int(input()), int(input())
    k = 0

    for i in range(1001):
        if (a * (i ** 3) + b * (i ** 2) + c * i + d) == 0 and i != e:
            k += 1

    return k


def main():

    print(roots_number())


if __name__ == "__main__":
    main()

def min_odd(_list):
    _list.sort()

    for i in _list:
        if i % 2 != 0:
            return i


def main():
    _list = list(map(int, input().split()))

    print(min_odd(_list))


if __name__ == "__main__":
    main()

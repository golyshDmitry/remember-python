def square_sum(n, i=1, s=0):

    if i == n:
        return s + i ** 2

    return s + square_sum(n, i + 1, i ** 2)


def main():
    n = int(input())

    print(square_sum(n))


if __name__ == "__main__":
    main()

def is_ascending(_list):
    temp = 0
    length = len(_list)

    if length == 1:
        return True

    i = _list[temp + 1] > _list[temp]

    temp += 1

    while i and temp < length - 1:
        i = _list[temp + 1] > _list[temp]
        temp += 1

    return i


def main():
    myList = list(map(int, input().split()))

    if is_ascending(myList):
        print("YES")
    else:
        print("NO")


if __name__ == "__main__":
    main()

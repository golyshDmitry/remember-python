def reverse_list(_list):
    _list.reverse()

    print(' '.join(map(str, _list)))


def main():
    _list = list(map(int, input().split()))

    reverse_list(_list)


if __name__ == "__main__":
    main()

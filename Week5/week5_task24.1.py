def reverse_list(_list):

    for i in range(len(_list) - 1, -1, -1):
        print(_list[i], end=' ')


def main():
    _list = list(map(int, input().split()))

    reverse_list(_list)


if __name__ == "__main__":
    main()

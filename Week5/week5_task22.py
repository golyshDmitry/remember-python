def min_positive(_list):
    el = 1001

    for i in _list:
        if 0 < i < el:
            el = i

    return el


def main():
    _list = list(map(int, input().split()))

    print(min_positive(_list))


if __name__ == "__main__":
    main()

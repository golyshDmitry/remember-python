def foo(a, b):

    for i in range(a, b + 1):
        print(i)


def main():
    a, b = int(input()), int(input())

    foo(a, b)


if __name__ == "__main__":
    main()

def max_composition_of_two(_list):

    m1 = _list.pop(_list.index(max(_list)))
    n1 = _list.pop(_list.index(min(_list)))

    m2 = max(_list)
    n2 = min(_list)

    if n1 * n2 > m1 * m2:
        return n1, n2
    else:
        return m2, m1


def main():
    _list = list(map(int, input().split()))

    print(*max_composition_of_two(_list))


if __name__ == "__main__":
    main()

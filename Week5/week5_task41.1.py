def compress_list(_list):
    length = len(_list)
    null_amount = _list.count(0)

    for i in range(length):
        if _list[i] != 0:
            _list.append(_list[i])

    for i in range(null_amount):
        _list.append(0)

    _list.reverse()

    for i in range(length):
        _list.pop()

    _list.reverse()

    return _list


def main():
    _list = list(map(int, input().split()))

    print(*compress_list(_list))


if __name__ == '__main__':
    main()

def cyclic_shift(_list):

    temp = _list[-1]

    for i in range(len(_list) - 1, 0, -1):
        _list[i] = _list[i - 1]

    _list[0] = temp

    return _list


def main():
    _list = list(map(int, input().split()))

    print(*cyclic_shift(_list))


if __name__ == "__main__":
    main()

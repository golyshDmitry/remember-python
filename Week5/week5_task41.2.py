def compress_list(_list):

    for i in range(len(_list)):
        if _list[i] == 0:
            _list.remove(0)
            _list.append(0)
            i -= 1

    return _list


def main():
    _list = list(map(int, input().split()))

    print(*compress_list(_list))


if __name__ == '__main__':
    main()

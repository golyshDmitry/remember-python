def index_of_max(_list):
    temp_max = _list[0]
    temp_index = 0

    for i in range(1, len(_list)):
        if _list[i] >= temp_max:
            temp_max = _list[i]
            temp_index = i

    return temp_max, temp_index


def main():
    myList = list(map(int, input().split()))

    print(* index_of_max(myList))


if __name__ == "__main__":
    main()

def different_elements(_list):
    k = i = 0
    length = len(_list)

    while i < length:
        i += _list.count(_list[i])
        k += 1

    return k


def main():
    _list = list(map(int, input().split()))

    print(different_elements(_list))


if __name__ == "__main__":
    main()

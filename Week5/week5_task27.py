def insert_element(_list, k, c):
    _list.append(c)

    for i in range(len(_list) - 1, k, -1):
        temp = _list[i]
        _list[i] = _list[i - 1]
        _list[i - 1] = temp

    return _list


def main():
    _list = list(map(int, input().split()))
    temp_list = list(map(int, input().split()))

    k, c = temp_list[0], temp_list[1]

    print(' '.join(map(str, insert_element(_list, k, c))))


if __name__ == "__main__":
    main()

def fact_sum(n):
    s = 0

    for i in range(1, n + 1):
        s += fact(i)

    return s


def fact(i):

    if i == 0:
        return 1

    return i * fact(i - 1)


def main():
    n = int(input())

    print(fact_sum(n))


if __name__ == "__main__":
    main()

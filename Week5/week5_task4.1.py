def square_sum(n):
    s = 0

    for i in range(1, n + 1):
        s += i ** 2

    return s


def main():
    n = int(input())

    print(square_sum(n))


if __name__ == "__main__":
    main()

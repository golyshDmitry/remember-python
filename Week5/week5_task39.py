def bowling(_list, l, r):

    for i in range(l - 1, r):
        _list[i] = '.'

    return _list


def main():
    temp = list(map(int, input().split()))
    n, k = temp[0], temp[1]

    skittles = []

    for i in range(n):
        skittles.append('I')

    for i in range(k):
        temp = list(map(int, input().split()))

        skittles = bowling(skittles, temp[0], temp[1])

    print(''.join(map(str, skittles)))


if __name__ == "__main__":
    main()

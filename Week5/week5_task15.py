def count_positive(_list):
    k = 0

    for i in _list:
        if i > 0:
            k += 1

    return k


def main():
    myList = list(map(int, input().split()))

    print(count_positive(myList))


if __name__ == "__main__":
    main()

def remove_element(_list, k):
    for i in range(k, len(_list) - 1):
        temp = _list[i]
        _list[i] = _list[i + 1]
        _list[i + 1] = temp

    _list.pop()

    return _list


def main():
    _list = list(map(int, input().split()))
    k = int(input())

    print(' '.join(map(str, remove_element(_list, k))))


if __name__ == "__main__":
    main()

def replace_near_elements(_list):

    for i in range(0, len(_list) - 1 - len(_list) % 2, 2):
        _list[i], _list[i + 1] = _list[i + 1], _list[i]

    return _list


def main():
    _list = list(map(int, input().split()))

    print(' '.join(map(str, replace_near_elements(_list))))


if __name__ == "__main__":
    main()

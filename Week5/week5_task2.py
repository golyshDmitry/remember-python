def foo(a, b):

    if a <= b:
        for i in range(a, b + 1):
            print(i, end=' ')
        return

    else:
        for i in range(a, b - 1, -1):
            print(i, end=' ')


def main():
    a, b = int(input()), int(input())

    foo(a, b)


if __name__ == "__main__":
    main()

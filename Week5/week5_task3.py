def foo(n):

    for i in range((10 ** n) - 1, 10 ** (n - 1) - 1, -2):
        print(i, end=' ')


def main():
    n = int(input())

    foo(n)


if __name__ == "__main__":
    main()

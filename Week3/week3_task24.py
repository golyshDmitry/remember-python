s = input()

i = 0
l = len(s) * 2 - 1

while i < l - 2:
    s = s[:i] + s[i:].replace(s[i], s[i] + '*', 1)
    i += 2

print(s)

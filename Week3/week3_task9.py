temp = 1
i = 0
sqr_sum = sum_ = 0

while temp != 0:
    temp = int(input())

    i += 1

    sqr_sum += temp ** 2
    sum_ += temp

i -= 1

print(((sqr_sum - sum_ ** 2 / i) / (i - 1)) ** 0.5)

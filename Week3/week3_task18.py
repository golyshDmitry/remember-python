s = input()

first = s.find('f')

if first == len(s) - 1:
    print(-1)

elif first == -1:
    print(-2)

else:
    second = s.find('f', first + 1)
    print(second)

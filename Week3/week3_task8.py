n = int(input())
x = float(input())

temp = 0
sum_ = 0
i = 0

while i <= n:
    temp = float(input())
    sum_ = (sum_ + temp) * x
    i += 1

if x != 0:
    print(sum_ / x)
else:
    print(sum_ + temp)

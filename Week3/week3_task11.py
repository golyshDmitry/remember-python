a, b, c = float(input()), float(input()), float(input())

if a != 0:
    d = b ** 2 - 4 * a * c

    if d > 0:
        x1 = (- b + d ** 0.5) / 2 / a
        x2 = (- b - d ** 0.5) / 2 / a

        if x1 >= x2:
            print(2, x2, x1)
        else:
            print(2, x1, x2)

    elif d == 0:
        print(1, - b / 2 / a)

    else:
        print(0)

else:
    if a == 0 and b != 0 and c != 0:
        print(1, -c / b)

    elif a == 0 and b != 0 and c == 0:
        print(1, 0)

    elif a == 0 and b == 0 and c == 0:
        print(3)

    elif a == 0 and b == 0 and c != 0:
        print(0)

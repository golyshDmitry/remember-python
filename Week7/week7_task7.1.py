def guess_the_number():
    n = int(input())
    possible = set(i for i in range(1, n + 1))
    temp_set = set()

    while True:
        temp = input()

        if temp == "YES":
            possible &= temp_set
        elif temp == "NO":
            possible -= temp_set
        elif temp == "HELP":
            break
        else:
            temp_set = set(map(int, temp.split()))

    need_to_remove = list()

    for i in possible:
        if i > n:
            need_to_remove.append(i)

    for i in need_to_remove:
        possible.remove(i)

    return sorted(possible)


def main():
    print(*guess_the_number())


if __name__ == '__main__':
    main()

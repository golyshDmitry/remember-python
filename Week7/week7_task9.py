def polyglots(set_list):
    everyone_knows = set(set_list[0])
    all_languages = set(set_list[0])

    for i in range(1, len(set_list)):
        everyone_knows &= set(set_list[i])
        all_languages |= set(set_list[i])

    print(len(everyone_knows))
    for i in everyone_knows:
        print(i)

    print(len(all_languages))
    for i in all_languages:
        print(i)

    return


def main():
    set_list = []
    temp_set = set()
    n = int(input())

    for i in range(n):
        m = int(input())
        for j in range(m):
            temp_set.add(input())
        set_list.append(list(temp_set))
        temp_set.clear()

    polyglots(set_list)


if __name__ == '__main__':
    main()

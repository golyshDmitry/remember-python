def unique_elements(_list):
    return len(set(_list))


def main():
    _list = list(map(int, input().split()))

    print(unique_elements(_list))


if __name__ == '__main__':
    main()

def count_words(input_file):
    my_dict = dict()
    answer = list()

    for line in input_file:
        temp = list(line.split())
        for i in temp:
            answer.append(my_dict.setdefault(i, 0))
            my_dict[i] += 1

    return answer


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    print(*count_words(input_file))

    input_file.close()


if __name__ == '__main__':
    main()

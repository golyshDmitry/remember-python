def mr_president(input_file):
    _list = list()
    my_dict = dict()
    sum_ = 0

    for line in input_file:
        temp = list(line.split('\n'))
        my_dict[temp[0]] = my_dict.setdefault(temp[0], 0) + 1

    for i in my_dict:
        sum_ += my_dict[i]

    for key, value in my_dict.items():
        _list.append((value, key))

    _list.sort(key=lambda j: (-j[0], j[1]))

    if _list[0][0] / sum_ > 0.5:
        return _list[0][1],
    else:
        return _list[0][1], _list[1][1]


def main():
    input_file = open('input.txt', 'r', encoding='utf8')
    output_file = open('output.txt', 'w', encoding='utf8')

    for i in mr_president(input_file):
        print(i, file=output_file)

    input_file.close()
    output_file.close()


if __name__ == '__main__':
    main()

from math import floor


def bank_accounts(input_file):
    balance = list()
    accounts = dict()

    for line in input_file:
        temp = line.split()
        if temp[0] == "BALANCE":
            if temp[1] in accounts:
                balance.append(accounts[temp[1]])
            else:
                balance.append("ERROR")

        elif temp[0] == "DEPOSIT":
            accounts[temp[1]] = accounts.setdefault(temp[1], 0) + int(temp[2])

        elif temp[0] == "WITHDRAW":
            accounts[temp[1]] = accounts.setdefault(temp[1], 0) - int(temp[2])

        elif temp[0] == "TRANSFER":
            accounts[temp[1]] = accounts.setdefault(temp[1], 0) - int(temp[3])
            accounts[temp[2]] = accounts.setdefault(temp[2], 0) + int(temp[3])

        elif temp[0] == "INCOME":
            for key in accounts:
                if accounts[key] > 0:
                    accounts[key] += floor(accounts[key] * int(temp[1]) / 100)

    return balance


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    for i in bank_accounts(input_file):
        print(i)

    input_file.close()


if __name__ == '__main__':
    main()

import sys


def guess_the_number():
    n_entered = False
    n = 0
    temp_set = set()
    yes = list()
    no = list()

    for line in sys.stdin:

        if line[0] == "Y":
            yes.append(temp_set)
        elif line[0] == "N":
            no.append(temp_set)
        elif line[0] == "H":
            break
        elif not n_entered:
            temp_list = list(map(int, line.split()))
            n = temp_list[0]
            n_entered = True
        else:
            temp_set = set(map(int, line.split()))

    if len(yes) == 0:
        answer = set(i for i in range(1, n + 1))

        for i in no:
            answer -= i
        return answer

    for i in range(len(yes) - 1):
        yes[i + 1] &= yes[i]

    for i in no:
        yes[-1] -= i

    need_to_remove = list()

    for i in yes[-1]:
        if i > n:
            need_to_remove.append(i)

    for i in need_to_remove:
        yes[-1].remove(i)

    return sorted(yes[-1])


def main():
    print(*guess_the_number())


if __name__ == '__main__':
    main()

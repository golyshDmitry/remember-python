def family_tree(n):
    tree = dict()
    answer = list()

    for i in range(n - 1):
        temp = input().split()
        tree[temp[0]] = temp[1]

    root = ''

    for i in sorted(tree):
        k = 1
        j = i
        while True:
            if tree[j] in tree:
                j = tree[j]
                k += 1
            else:
                root = tree[j]
                answer.append((i, k))
                break

    answer.append((root, 0))

    return sorted(answer)


def main():
    n = int(input())

    for i in family_tree(n):
        print(*i)


if __name__ == '__main__':
    main()

def count_words(input_file):
    words = set()

    for line in input_file:
        temp = line.split()

        for i in temp:
            words.add(i)

    return len(words)


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    print(count_words(input_file))

    input_file.close()


if __name__ == '__main__':
    main()

import sys


def accent_test(n):
    vocabulary = dict()
    my_list = list()
    k = 0

    for i in range(n):
        word = input()
        if word.lower() not in vocabulary:
            vocabulary[word.lower()] = []
        vocabulary[word.lower()].append(word)

    for line in sys.stdin:
        temp = list(line.split())
        for i in temp:
            my_list.append(i)

    for i in my_list:
        if i.lower() in vocabulary:
            if i not in vocabulary[i.lower()]:
                k += 1
        else:
            c = 0
            for j in i:
                if j.isupper():
                    c += 1
            if c != 1:
                k += 1

    return k


def main():
    n = int(input())

    print(accent_test(n))


if __name__ == '__main__':
    main()

def telephone_numbers(new, numbers):
    for i in range(len(numbers)):
        if len(numbers[i][0]) == len(new):
            if len(numbers[i][0]) == 11:
                if numbers[i][0][1:] == new[1:]:
                    numbers[i][1] = "YES"
            else:
                if numbers[i][0] == new:
                    numbers[i][1] = "YES"
        elif len(numbers[i][0]) > len(new) and numbers[i][0][1:] == new:
            numbers[i][1] = "YES"
        elif new[1:] == numbers[i][0]:
            numbers[i][1] = "YES"

    return


def numbers_only(s):
    numbers = ''

    for i in s:
        if i.isdigit():
            numbers += i

    if len(numbers) == 7:
        numbers = "495" + numbers

    return numbers


def main():
    new_number = numbers_only(input())
    numbers = list()

    for i in range(3):
        numbers.append([numbers_only(input()), "NO"])

    telephone_numbers(new_number, numbers)

    for i in range(len(numbers)):
        print(numbers[i][1])


if __name__ == '__main__':
    main()

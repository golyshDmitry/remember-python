def most_frequent_word(input_file):
    word = ["", 0]
    my_dict = dict()

    for line in input_file:
        temp = list(line.split())
        for i in temp:
            my_dict[i] = my_dict.setdefault(i, 0) + 1

    for key in sorted(my_dict):
        if my_dict[key] > word[1]:
            word[0] = key
            word[1] = my_dict[key]
        elif my_dict[key] == word[1]:
            if key < word[0]:
                word[0] = key
                word[1] = my_dict[key]

    return word[0]


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    print(most_frequent_word(input_file))

    input_file.close()


if __name__ == '__main__':
    main()

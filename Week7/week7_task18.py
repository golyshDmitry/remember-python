def frequency_analysis(input_file):
    my_dict = dict()
    answer = list()

    for line in input_file:
        temp = list(line.split())

        for i in temp:
            my_dict[i] = my_dict.setdefault(i, 0) + 1

    for key, value in my_dict.items():
        answer.append((-value, key))

    return sorted(answer)


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    for i in frequency_analysis(input_file):
        print(i[1])

    input_file.close()


if __name__ == '__main__':
    main()

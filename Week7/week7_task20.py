def sales(input_file):
    customers = dict()

    for line in input_file:
        temp = list(line.split())

        if temp[0] not in customers:
            customers[temp[0]] = {}
        customers[temp[0]][temp[1]] = customers[temp[0]].setdefault(
            temp[1], 0) + int(temp[2])

    print(customers)

    return customers


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    customers = sales(input_file)
    for i in sorted(customers):
        print(i, ':', sep='')
        for j in sorted(customers[i]):
            print(j, customers[i][j])

    input_file.close()


if __name__ == '__main__':
    main()

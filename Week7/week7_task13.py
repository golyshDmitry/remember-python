def main():
    my_dict = dict()

    for i in range(int(input())):
        line = list(input().split())

        for j in range(1, len(line)):
            my_dict[line[j]] = line[0]

    answer = list()

    for i in range(int(input())):
        line = input()
        answer.append(my_dict[line])

    for i in answer:
        print(i)


if __name__ == '__main__':
    main()

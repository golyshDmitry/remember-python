def synonyms_dict(word, my_dict):
    for key, value in my_dict.items():
        if key == word:
            return value
        elif value == word:
            return key

    return "no such a word"


def main():
    my_dict = dict()

    for i in range(int(input())):
        line = list(input().split())
        my_dict[line[0]] = line[1]

    print(synonyms_dict(input(), my_dict))


if __name__ == '__main__':
    main()

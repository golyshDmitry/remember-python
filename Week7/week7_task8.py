def give_the_answer(n):
    possible = set(i for i in range(1, n + 1))
    answer = list()

    while True:
        temp = input()

        if temp == "HELP":
            break
        else:
            temp_set = set(map(int, temp.split()))
            if len(possible & temp_set) > len(possible) // 2:
                possible &= temp_set
                answer.append("YES")
            else:
                possible -= temp_set
                answer.append("NO")

    for i in answer:
        print(i)

    print(*sorted(possible))

    return


def main():
    n = int(input())
    give_the_answer(n)


if __name__ == '__main__':
    main()

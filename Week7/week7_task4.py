def met_previously(_list):
    prev_len = 0
    s = list()
    temp_set = set()

    for i in range(len(_list)):
        temp_set.add(_list[i])

        if len(temp_set) == prev_len:
            s.append("YES")
        else:
            s.append("NO")

        prev_len = len(temp_set)

    return s


def main():
    _list = list(map(int, input().split()))

    for i in met_previously(_list):
        print(i)


if __name__ == '__main__':
    main()

def intersection(a, b, c, d):
    if a > b:
        a, b = b, a
    if c > d:
        c, d = d, c

    first = set(i for i in range(a, b + 1))
    second = set(i for i in range(c, d + 1))

    return len(first & second)


def main():
    _list = list(map(int, input().split()))

    print(intersection(_list[0], _list[1], _list[2], _list[3]))


if __name__ == '__main__':
    main()

def strikes(n, parties_strikes):
    year = set(i for i in range(1, n + 1) if i % 7 not in (0, 6))
    k = 0

    for i in range(len(parties_strikes)):
        day = parties_strikes[i][0]
        while day <= n:
            if day in year:
                year.remove(day)
                k += 1
            day += parties_strikes[i][1]

    return k


def main():
    temp = list(map(int, input().split()))
    n = temp[0]
    k = temp[1]

    parties_strikes = list()

    for i in range(k):
        temp = list(map(int, input().split()))
        parties_strikes.append((temp[0], temp[1]))

    print(strikes(n, parties_strikes))


if __name__ == '__main__':
    main()

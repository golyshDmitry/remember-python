def election_of_deputies(input_file):
    my_dict = dict()
    names = list()
    sum_ = 0

    for line in input_file:
        temp = list(line.split())
        names.append(' '.join(temp[:-1]))
        if names[-1] not in my_dict:
            my_dict[names[-1]] = [0]
        my_dict[names[-1]][0] += int(temp[-1])

    for value in my_dict.values():
        sum_ += value[0]

    first_private = sum_ / 450
    places = 0

    for value in my_dict.values():
        value.append(value[0] // first_private)
        value.append(value[0] % first_private)
        places += value[1]

    if places == 450:
        for i in names:
            print(i, int(my_dict[i][1]))
        return

    for i in sorted(my_dict, key=lambda q: (my_dict[q][2],
                                            my_dict[q][0]), reverse=True):
        my_dict[i][1] += 1
        places += 1
        if places == 450:
            break

    for i in names:
        print(i, int(my_dict[i][1]))

    return


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    election_of_deputies(input_file)

    input_file.close()


if __name__ == '__main__':
    main()

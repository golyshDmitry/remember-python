def elections(input_file):
    my_dict = dict()

    for line in input_file:
        temp = list(line.split())
        my_dict[temp[0]] = my_dict.setdefault(temp[0], 0) + int(temp[1])

    return my_dict


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    my_dict = elections(input_file)

    for i in sorted(my_dict):
        print(i, my_dict[i])

    input_file.close()


if __name__ == '__main__':
    main()

def cubes(input_file):
    answer = list()
    ann = set()
    borya = set()
    n = m = i = 0

    for line in input_file:
        temp = list(map(int, line.split()))

        if len(temp) == 2:
            n = temp[0]
            m = temp[1]
            continue

        if i < n:
            ann.add(temp[0])
        else:
            borya.add(temp[0])

        i += 1

    temp_set = ann.intersection(borya)

    answer.append(len(temp_set))
    answer.append(sorted(temp_set))

    temp_set = ann - borya

    answer.append(len(temp_set))
    answer.append(sorted(temp_set))

    temp_set = borya - ann

    answer.append(len(temp_set))
    answer.append(sorted(temp_set))

    return answer


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    for i in cubes(input_file):
        if type(i) == int:
            print(i)
        else:
            print(*i)

    input_file.close()


if __name__ == '__main__':
    main()

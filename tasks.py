def arithmetic(a, b, op):
    if op == "+":
        return a + b
    elif op == "-":
        return a - b
    elif op == "*":
        return a * b
    elif op == "/":
        return a / b
    else:
        return "Неизвестная операция"


def is_year_leap(n):
    if (n % 4 == 0 and n % 100 != 0) or (n % 400 == 0):
        return "True"
    else:
        return "False"


def main():
    while True:
        a = int(input())
        b = int(input())
        c = input()
        print(arithmetic(a, b, c))
        if type(arithmetic(a, b, c)) == str:
            break
    n = int(input())
    print(is_year_leap(n))


if __name__ == "__main__":
    main()

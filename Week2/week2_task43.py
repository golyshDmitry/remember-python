n = int(input())

while n > 0:
    print(n % 10, sep='', end='')
    n = n // 10

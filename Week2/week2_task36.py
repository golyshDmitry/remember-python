i = 0
temp = 1

while temp != 0:
    temp = int(input())
    if temp % 2 == 0:
        i += 1

print(i - 1)

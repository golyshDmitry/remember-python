n = int(input())

i = 0
k = 1

while k < n:
    k = k * 2
    i += 1

print(i)

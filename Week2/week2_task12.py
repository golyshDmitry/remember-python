x1 = int(input())
y1 = int(input())
x2 = int(input())
y2 = int(input())

desk = [[0] * 8 for i in range(8)]

start = (8 - y1, x1 - 1)
end = (8 - y2, x2 - 1)


def bfs(s, e):
    q = list()
    q.append(s)
    while True:
        desk[q[0][0]][q[0][1]] = 1
        if q[0][0] == e[0] and q[0][1] == e[1]:
            return "YES"
        if q[0][0] - 1 >= 0:
            if q[0][1] - 1 >= 0 and desk[q[0][0] - 1][q[0][1] - 1] != 1:
                q.append((q[0][0] - 1, q[0][1] - 1))
            if q[0][1] + 1 <= 7 and desk[q[0][0] - 1][q[0][1] + 1] != 1:
                q.append((q[0][0] - 1, q[0][1] + 1))
        q.pop(0)
        if len(q) == 0:
            return "NO"


print(bfs(start, end))

n = int(input())

k = 0
j = 1

i = 2
f = 0

if n < 2:
    if n == 1:
        print(1)
    else:
        print(0)
else:
    while i <= n:
        f = k + j
        k = j
        j = f
        i += 1

    print(f)

a, b, c, d, e, f = int(input()), int(input()), int(input()),\
                   int(input()), int(input()), int(input())

box_1 = [a, b, c]
box_2 = [d, e, f]

box_1.sort()
box_2.sort()

if (box_1[0] == box_2[0]) and (box_1[1] == box_2[1]) and\
        (box_1[2] == box_2[2]):
    print("Boxes are equal")
elif (box_1[0] <= box_2[0]) and (box_1[1] <= box_2[1]) and\
        (box_1[2] <= box_2[2]):
    print("The first box is smaller than the second one")
elif (box_1[0] >= box_2[0]) and (box_1[1] >= box_2[1]) and\
        (box_1[2] >= box_2[2]):
    print("The first box is larger than the second one")
else:
    print("Boxes are incomparable")

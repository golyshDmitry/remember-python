a, b = int(input()), int(input())


def div_two(x):
    x = x // 2
    print(":2")
    return x


def min_one_div_two(x, y):
    if (x - 1) // 2 >= y:
        x = (x - 1) // 2
        print("-1")
        print(":2")
    return x


if a == 2 and b == 1:
    print(":2")
    exit()

while a != b:

    if a // 2 < b:
        a -= 1
        print("-1")
    elif a % 2 == 0:
        a = div_two(a)
    else:
        a = min_one_div_two(a, b)

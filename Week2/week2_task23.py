a, b, c, d, e, f = int(input()), int(input()), int(input()),\
                   int(input()), int(input()), int(input())

l1 = b - a
l2 = d - c
l3 = f - e

line = [a, b, c, d, e, f]
line.sort()

matches = []
used = [0] * 6
can = [0] * 3
not_col = [0] * 3

for i in line:
    if id(i) == id(a) and used[0] == 0:
        matches.append('a')
        used[0] = 1
    elif id(i) == id(b) and used[1] == 0:
        matches.append('b')
        used[1] = 1
    elif id(i) == id(c) and used[2] == 0:
        matches.append('c')
        used[2] = 1
    elif id(i) == id(d) and used[3] == 0:
        matches.append('d')
        used[3] = 1
    elif id(i) == id(e) and used[4] == 0:
        matches.append('e')
        used[4] = 1
    elif id(i) == id(f) and used[5] == 0:
        matches.append('f')
        used[5] = 1

collision = 3

for i in range(6):
    if (i % 2 == 1) and (ord(matches[i]) - 1 == ord(matches[i - 1])) \
            and ((i == 5) or (line[i] - line[i + 1] < 0)) and \
            (matches[i] == 'b' or matches[i] == 'd' or matches[i] == 'f')\
            and ((i == 1) or (line[i - 1] != line[i - 2])):
        collision -= 1
        if matches[i] == 'b':
            not_col[0] = 1
        elif matches[i] == 'd':
            not_col[1] = 1
        else:
            not_col[2] = 1

if collision == 0:
    if (line[1] - line[0]) >= (line[4] - line[3]):
        if matches[0] == 'a':
            can[0] = 1
        elif matches[0] == 'c':
            can[1] = 1
        else:
            can[2] = 1
    if (line[5] - line[4]) >= (line[2] - line[1]):
        if matches[-1] == 'b':
            can[0] = 1
        elif matches[-1] == 'd':
            can[1] = 1
        else:
            can[2] = 1
    for i in range(3):
        if can[i] == 1:
            print(i + 1)
            exit()
    print(-1)

elif collision == 3:
    print(0)

elif collision == 2:
    if not_col[0] == 1:
        print(1)
    elif not_col[1] == 1:
        if l1 >= max(e - d, c - f):
            print(1)
        else:
            print(2)
    else:
        if l1 >= max(c - f, e - d):
            print(1)
        elif l2 >= max(a - f, e - b):
            print(2)
        else:
            print(3)

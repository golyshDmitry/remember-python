k, m, n = int(input()), int(input()), int(input())

num = (n // k) * 2 * m

if n <= k:
    print(m * 2)
elif n % k == 0:
    print(num)
elif n % k <= k / 2:
    print(num + m)
else:
    print(num + 2 * m)

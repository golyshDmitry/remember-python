temp = 1
k = 2
k_max = 2

a = []
b = []


def same_signs(x, y):
    if x == 0 or y == 0:
        return 0
    elif ((abs(x) == x) and (abs(y) == y)) or \
            ((abs(x) == -x) and (abs(y) == -y)):
        return 1
    else:
        return 0


while temp != 0:
    temp = int(input())
    a.append(temp)

for i in range(1, len(a)):
    b.append(a[i] - a[i - 1])

b.pop()

if b.count(0) == len(b):
    print(1)
    exit()

for i in range(1, len(b)):
    if same_signs(b[i], b[i - 1]) == 1:
        k += 1
    else:
        k = 2

    if k > k_max:
        k_max = k

print(k_max)

n = int(input())

i = 1
_sum = 0

while i <= n:
    _sum += i * i
    i += 1

print(_sum)

l1, w1, h1, l2, w2, h2, lc, wc, hc = int(input()), int(input()), \
            int(input()), int(input()), int(input()), int(input()), \
            int(input()), int(input()), int(input())

H = max(h1, h2)
H_ = h1 + h2

if (l1 + l2 <= lc) and (max(w1, w2) <= wc) and (H <= hc):
    print("YES")
elif (l1 + w2 <= lc) and (max(w1, l2) <= wc) and (H <= hc):
    print("YES")
elif (w1 + l2 <= lc) and (max(l1, w2) <= wc) and (H <= hc):
    print("YES")
elif (w1 + w2 <= lc) and (max(l1, l2) <= wc) and (H <= hc):
    print("YES")

elif (max(l1, l2) <= lc) and (w1 + w2 <= wc) and (H <= hc):
    print("YES")
elif (max(w1, l2) <= lc) and (l1 + w2 <= wc) and (H <= hc):
    print("YES")
elif (max(l1, w2) <= lc) and (w1 + l2 <= wc) and (H <= hc):
    print("YES")
elif (max(w1, w2) <= lc) and (l1 + l2 <= wc) and (H <= hc):
    print("YES")

elif (max(l1, l2) <= lc) and (max(w1, w2) <= wc) and (H_ <= hc):
    print("YES")
elif (max(l1, w2) <= lc) and (max(w1, l2) <= wc) and (H_ <= hc):
    print("YES")
elif (max(w1, l2) <= lc) and (max(l1, w2) <= wc) and (H_ <= hc):
    print("YES")
elif (max(w1, w2) <= lc) and (max(l1, l2) <= wc) and (H_ <= hc):
    print("YES")

else:
    print("NO")

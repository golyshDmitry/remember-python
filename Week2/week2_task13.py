a = int(input())
b = int(input())
c = int(input())

g = max(a, b, c)

if g >= (a + b + c - g):
    print("impossible")
elif g**2 == a**2 + b**2 + c**2 - g**2:
    print("rectangular")
elif g**2 < a**2 + b**2 + c**2 - g**2:
    print("acute")
else:
    print("obtuse")

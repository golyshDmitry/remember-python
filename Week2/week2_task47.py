temp = 1
a = []
b = []
c = []

while temp != 0:
    temp = int(input())

    a.append(temp)

a.pop()

for i in range(1, len(a) - 1):
    if a[i] > a[i - 1] and a[i] > a[i + 1]:
        b.append(i)

if len(b) < 2:
    print(0)
    exit()

for i in range(1, len(b)):
    c.append(b[i] - b[i - 1])

c.sort()

print(c[0])

n = int(input())

i = 1
k = 0


def reverse_(x):
    rev = ""
    while x > 0:
        rev += str(x % 10)
        x = x // 10
    return rev


while i <= n:
    if i == int(reverse_(i)):
        k += 1
    i += 1

print(k)

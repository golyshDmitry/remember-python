max_1 = max_2 = 0
temp = 1

while temp != 0:
    temp = int(input())

    if temp > max_1:
        max_2 = max_1
        max_1 = temp

    elif temp > max_2:
        max_2 = temp

print(max_2)

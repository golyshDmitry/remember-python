x1 = int(input())
y1 = int(input())
x2 = int(input())
y2 = int(input())


def sign_x(x):
    if x > 0:
        return 1
    else:
        return 0


if sign_x(x1) == sign_x(x2) and sign_x(y1) == sign_x(y2):
    print("YES")
else:
    print("NO")

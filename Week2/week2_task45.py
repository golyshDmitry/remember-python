temp = 1
temp_max = _max = 1
prev = 0

while temp != 0:
    temp = int(input())

    if temp == prev:
        temp_max += 1
    else:
        temp_max = 1

    if temp_max > _max:
        _max = temp_max

    prev = temp

print(_max)

max_ = 0
i = 0
temp = 1

while temp != 0:
    temp = int(input())

    if temp > max_:
        max_ = temp
        i = 1

    if temp == max_:
        i += 1

print(i - 1)

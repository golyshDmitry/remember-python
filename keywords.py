import keyword


def main():
    print("List of Python keywords: ", keyword.kwlist)
    a = [4, 5, 6]
    b = [1, 2, 3]
    print(id(a))
    print(id(b))
    a = b
    print(id(a))
    a.pop()
    b.append("suck")
    print(a, ' ', b)
    print(id(a))
    print(id(b))
    print()
    c = "string1"
    d = "string2"
    print(c)
    print(d)
    print(id(c))
    print(id(d))
    c = d
    print(c)
    print(d)
    print(id(c))
    print(id(d))
    d = d + "HI"
    print(c)
    print(d)
    print(id(c))
    print(id(d))


if __name__ == "__main__":
    main()
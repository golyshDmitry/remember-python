def elected_parties(input_file):
    parties = list()
    votes = list()
    x = parties
    answer = list()

    for line in input_file:
        temp = list(line.split())

        temp_2 = ""
        for i in range(len(temp)):
            temp_2 += temp[i] + ' '

        temp_2 = temp_2[:-1]

        if temp_2 == "PARTIES:":
            continue

        elif temp_2 == "VOTES:":
            x = votes
            continue

        x.append(temp_2)

    for i in parties:
        answer.append((votes.count(i), i))

    return sorted(answer, key=lambda q: (-q[0], q[1]))


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    for i in elected_parties(input_file):
        print(i[1])

    input_file.close()


if __name__ == '__main__':
    main()

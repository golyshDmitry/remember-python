def olympiad_results(participants):
    participants.sort(key=point, reverse=True)

    return participants


def point(a):
    return int(a[1])


def main():
    n = int(input())
    participants = list()

    for i in range(n):
        participants.append(input().split())

    olympiad_results(participants)

    for i in participants:
        print(i[0])


if __name__ == '__main__':
    main()

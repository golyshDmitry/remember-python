def max_number_of_pairs(n, _list):
    k = 0
    prev = -10

    if len(_list) == 0:
        return 0

    _list.sort()

    for i in range(len(_list)):
        if _list[i] >= n and _list[i] - prev >= 3:
            prev = _list[i]
            k += 1

    return k


def main():
    n = int(input())
    _list = list(map(int, input().split()))

    print(max_number_of_pairs(n, _list))


if __name__ == "__main__":
    main()

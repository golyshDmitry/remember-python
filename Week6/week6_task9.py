def count_sort(_list):
    a = [0] * 101

    for i in _list:
        a[i] += 1

    for i in range(len(a)):
        if a[i] != 0:
            print((str(i) + ' ') * a[i], end='')

    return


def main():
    _list = list(map(int, input().split()))

    count_sort(_list)


if __name__ == '__main__':
    main()

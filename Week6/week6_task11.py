def winners(input_file):
    winners_list = [0, 0, 0]

    for line in input_file:
        temp = list(line.split())

        if winners_list[int(temp[2]) - 9] < int(temp[3]):
            winners_list[int(temp[2]) - 9] = int(temp[3])

    return winners_list


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    print(*winners(input_file))

    input_file.close()


if __name__ == '__main__':
    main()

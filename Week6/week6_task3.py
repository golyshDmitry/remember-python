def sorted_list(_list):
    return sorted(_list)


def main():
    n = int(input())

    _list = list(map(int, input().split()))

    print(*sorted_list(_list))


if __name__ == "__main__":
    main()

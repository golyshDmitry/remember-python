def sort_surnames(input_file, output_file):
    lines = list()

    for line in input_file:
        temp = list(line.split())
        lines.append(temp[0] + ' ' + temp[1] + ' ' + temp[3])

    lines.sort()

    for i in lines:
        print(i, file=output_file)


def main():
    input_file = open('input.txt', 'r', encoding='utf8')
    output_file = open('output.txt', 'w', encoding='utf8')

    sort_surnames(input_file, output_file)

    input_file.close()
    output_file.close()


if __name__ == '__main__':
    main()

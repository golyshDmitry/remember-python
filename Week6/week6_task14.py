def passing_score(input_file):
    first_score = n = 0
    applicants = list()

    for line in input_file:
        temp = list(line.split())

        if len(temp) == 1:
            n = int(temp[0])
            continue

        for i in range(len(temp)):
            if 47 < ord(temp[i][0]) < 58:
                first_score = i
                break

        if int(temp[first_score]) >= 40 and int(temp[first_score + 1]) >= 40 \
                and int(temp[first_score + 2]) >= 40:
            temp.append(sum((int(temp[first_score]),
                             int(temp[first_score + 1]),
                             int(temp[first_score + 2]))))
            applicants.append(temp)

    if len(applicants) <= n:
        return 0

    applicants.sort(key=take_sum, reverse=True)

    if applicants[n - 1][-1] == applicants[n][-1]:
        if applicants[n - 2][-1] != applicants[n - 1][-1]:
            return applicants[n - 2][-1]
        else:
            for i in range(n - 2, 0, -1):
                if applicants[i][-1] != applicants[i - 1][-1]:
                    return applicants[i - 1][-1]
            return 1

    return applicants[n - 1][-1]


def take_sum(a):
    return a[-1]


def main():
    input_file = open('input.txt', 'r', encoding='utf8')
    output_file = open('output.txt', 'w', encoding='utf8')

    print(passing_score(input_file), file=output_file, end='')

    input_file.close()
    output_file.close()


if __name__ == '__main__':
    main()

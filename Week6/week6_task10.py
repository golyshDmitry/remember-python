def keyboard(n, keys, sequence):
    a = [0] * n

    for i in sequence:
        a[i - 1] += 1

    for i in range(n):
        if a[i] > keys[i]:
            print("YES")
        else:
            print("NO")

    return


def main():
    n = int(input())
    keys = list(map(int, input().split()))

    int(input())
    sequence = list(map(int, input().split()))

    keyboard(n, keys, sequence)


if __name__ == '__main__':
    main()

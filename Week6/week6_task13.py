def number_of_winners(input_file):
    max_points = [[0, 0] for i in range(3)]

    for line in input_file:
        temp = list(line.split())

        if max_points[int(temp[2]) - 9][0] < int(temp[3]):
            max_points[int(temp[2]) - 9][0] = int(temp[3])
            max_points[int(temp[2]) - 9][1] = 1

        elif max_points[int(temp[2]) - 9][0] == int(temp[3]):
            max_points[int(temp[2]) - 9][1] += 1

    return max_points[0][1], max_points[1][1], max_points[2][1]


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    print(*number_of_winners(input_file))

    input_file.close()


if __name__ == '__main__':
    main()

def merge_lists(a, b):
    c = list()
    i = j = 0

    len_a = len(a)
    len_b = len(b)

    while i < len_a and j < len_b:
        if a[i] <= b[j]:
            c.append(a[i])
            i += 1
        else:
            c.append(b[j])
            j += 1

    if i == len_a:
        for k in range(j, len_b):
            c.append(b[k])
    else:
        for k in range(i, len_a):
            c.append(a[k])

    return c


def main():
    a = list(map(int, input().split()))
    b = list(map(int, input().split()))

    print(*merge_lists(a, b))


if __name__ == '__main__':
    main()

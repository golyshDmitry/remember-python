def average_mark(input_file):
    a = [[0, 0] for i in range(3)]

    for line in input_file:
        temp_list = list(line.split())
        a[int(temp_list[2]) - 9][0] += int(temp_list[3])
        a[int(temp_list[2]) - 9][1] += 1

    return a[0][0] / a[0][1], a[1][0] / a[1][1], a[2][0] / a[2][1]


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    print(*average_mark(input_file))

    input_file.close()


if __name__ == "__main__":
    main()

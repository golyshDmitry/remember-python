def elected_parties(input_file):
    el_p = list()
    parties = list()
    votes = list()
    x = parties
    answer = list()

    for line in input_file:
        temp = list(line.split())

        temp_2 = ""
        for i in range(len(temp)):
            temp_2 += temp[i] + ' '

        temp_2 = temp_2[:-1]

        if temp_2 == "PARTIES:":
            continue

        elif temp_2 == "VOTES:":
            x = votes
            continue

        x.append(temp_2)

    votes.sort(reverse=True)

    i = 0
    length_v = len(votes)

    while i < length_v:
        n = votes.count(votes[i])
        if n / length_v >= 0.07:
            el_p.append(votes[i])
        i += n

    for i in parties:
        for j in el_p:
            if i == j:
                answer.append(i)
                break

    return answer


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    for i in elected_parties(input_file):
        print(i)

    input_file.close()


if __name__ == '__main__':
    main()

def average_mark(input_file):
    av_marks = [[0, 0] for i in range(3)]

    for line in input_file:
        temp_list = list(line.split())
        av_marks[int(temp_list[2]) - 9][0] += int(temp_list[3])
        av_marks[int(temp_list[2]) - 9][1] += 1

    return av_marks[0][0] / av_marks[0][1], av_marks[1][0] / av_marks[1][1], \
           av_marks[2][0] / av_marks[2][1]


def main():
    input_file = open('input.txt', 'r', encoding='utf8')
    output_file = open('output.txt', 'w', encoding='utf8')

    print(*average_mark(input_file), file=output_file)

    input_file.close()
    output_file.close()


if __name__ == "__main__":
    main()

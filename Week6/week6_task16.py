def max_score_of_looser(input_file):
    max_score_of_looser_list = [[], [], []]
    answer = list()

    for line in input_file:
        temp = list(line.split())

        max_score_of_looser_list[int(temp[2]) - 9].append(int(temp[3]))

    for i in max_score_of_looser_list:
        prev_max = max_ = 0

        for j in i:
            if j > max_:
                prev_max = max_
                max_ = j
            elif prev_max < j < max_:
                prev_max = j

        answer.append(prev_max)

    return answer


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    print(*max_score_of_looser(input_file))

    input_file.close()


if __name__ == '__main__':
    main()

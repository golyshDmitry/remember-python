def civil_defence(n, cities, shelters):
    nearest_shelters = [0] * n
    min_distance = 1000000001
    temp_shelter = 0

    cities.sort()
    shelters.sort()

    for i in range(len(cities)):
        for j in range(temp_shelter, len(shelters)):
            if abs(shelters[j][0] - cities[i][0]) > min_distance:
                break
            if abs(shelters[j][0] - cities[i][0]) < min_distance:
                min_distance = abs(shelters[j][0] - cities[i][0])
                temp_shelter = j

        nearest_shelters[cities[i][1]] = shelters[temp_shelter][1] + 1
        min_distance = 1000000001

    return nearest_shelters


def main():
    n = int(input())
    a = list(map(int, input().split()))

    m = int(input())
    b = list(map(int, input().split()))

    cities = list()
    shelters = list()

    for i in range(len(a)):
        cities.append((a[i], i))

    for i in range(len(b)):
        shelters.append((b[i], i))

    print(*civil_defence(n, cities, shelters))


if __name__ == "__main__":
    main()

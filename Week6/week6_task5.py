def number_of_users(s, n, data):
    k = sum_ = i = 0
    length = len(data)

    data.sort()

    while sum_ < s and i < length:
        sum_ += data[i]

        if sum_ < s:
            k += 1

        i += 1

    return k


def main():
    temp_list = list(map(int, input().split()))

    s = temp_list[0]
    n = temp_list[1]

    data = list()

    for i in range(n):
        data.append(int(input()))

    print(number_of_users(s, n, data))


if __name__ == "__main__":
    main()

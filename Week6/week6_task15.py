def schools_with_max_participants(input_file):
    participants = [0] * 99
    max_participants = list()
    answer = list()

    for line in input_file:
        temp = list(line.split())

        participants[int(temp[2]) - 1] += 1

    for i in range(len(participants)):
        if participants[i] != 0:
            max_participants.append([participants[i], i + 1])

    max_p = 0

    for i in max_participants:
        if i[0] > max_p:
            max_p = i[0]

    for i in max_participants:
        if i[0] == max_p:
            answer.append(i[1])

    answer.sort()

    return answer


def main():
    input_file = open('input.txt', 'r', encoding='utf8')

    print(*schools_with_max_participants(input_file), end='')


if __name__ == '__main__':
    main()

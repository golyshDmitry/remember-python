def min_cost_of_taxi(distance, prices):
    k = 0

    distance.sort()
    prices.sort()

    length = len(distance)

    for i in range(length):
        k += distance[i] * prices[length - 1 - i]

    return k


def main():
    distance = list(map(int, input().split()))
    prices = list(map(int, input().split()))

    print(min_cost_of_taxi(distance, prices))


if __name__ == '__main__':
    main()

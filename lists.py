def main():
    arr = list(range(1, 11))
    print(arr)
    arr.insert(0, "Suck")
    print(arr)
    print(arr[1])
    arr.remove(3)
    print(arr)
    try:
        a = arr.index("suck")
        print(a)
    except ValueError:
        print("Такого элемента не найдено!")
    finally:
        print(arr)
        arr.reverse()
        print(arr)
    try:
        print(max(arr), " ", min(arr))
    except TypeError:
        print("Невозможное сравнение.")
    finally:
        print("Пока!")
    print(arr.count(3))
    print(int(3)+3.5)

    a = [1, 2, 3]
    b = list(a)
    print(id(a) == id(b))
    a.pop()
    print(a)
    print(b)


if __name__ == "__main__":
    main()

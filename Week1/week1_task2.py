def print_peng(num):
    peng1 = "   _~_    "
    peng2 = "  (o o)   "
    peng3 = " /  V  \  "
    peng4 = "/(  _  )\\ "
    peng5 = "  ^^ ^^   "
    penguin = [peng1*num, peng2*num, peng3*num, peng4*num, peng5*num]
    for peng in penguin:
        print(peng)


n = int(input())
print_peng(n)

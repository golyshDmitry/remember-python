n = int(input())
today = n % 86400
hours = today // 3600
minutes = (today % 3600) // 60
seconds = (today % 3600) % 60
print(hours, ":", minutes // 10, minutes % 10, ":",
      seconds // 10, seconds % 10, sep='')
